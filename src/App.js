import Search from "./components/Search";
import DigimonList from "./components/DigimonList";
import GlobalStyle from "./styles/global";
import { Container, Header } from "./AppStyle";
import Logo from "./assets/logo.png";

function App() {
  return (
    <Container>
      <GlobalStyle />
      <Header>
        <img src={Logo} alt="logo"></img>
        <Search />
      </Header>
      <DigimonList />
    </Container>
  );
}

export default App;
