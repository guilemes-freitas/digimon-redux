import styled from "styled-components";

export const Container = styled.div`
  width: 100vw;
  height: 100vh;
  display: flex;
  flex-direction: column;
  align-items: center;
`;

export const Header = styled.header`
  display: flex;
  align-items: center;
  margin-bottom: 50px;
  > img {
    min-width: 70px;
    max-width: 115px;
    width: 15vw;
    position: absolute;
    left: 5px;
    top: 5px;
  }
`;
