import { createGlobalStyle } from "styled-components";

export default createGlobalStyle`
    * {
        margin: 0;
        padding: 0;
        box-sizing: border-box;
        outline: 0;
    }

    :root {
        --white: #f5f5f5;
        --black: #0c0d0d;
        --red: #e03f2d;
        --yellow: #fabe33;
        --stroke: #732d2d;
        --green: #80DFA8;
        --light-green: #E6FB9F;
        --light-brown: #A89E98;
    }

    body{
        background: var(--white);
        color: var(--black);
    }
    body,input,button{
        font-family: 'PT Serif', serif;
        font-size: 1rem;
    }

    h1,h2,h3,h4,h5,h6{
        font-family: "Orbitron", sans-serif;
        font-weight: 700;
    }

    button{
        cursor: pointer;
    }

    a{
        text-decoration: none;
    }
`;
