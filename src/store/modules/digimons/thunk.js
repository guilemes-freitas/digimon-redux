import api from "../../../services/api";

import { addDigimon } from "./actions";

const addDigimonsThunk = (digimon, setError) => {
  return (dispatch) => {
    api
      .get(`/api/digimon/name/${digimon}`)
      .then((response) => {
        const digimonResponse = response.data[0];
        dispatch(addDigimon(digimonResponse));
      })
      .catch((err) => {
        setError(true);
      });
  };
  //Aqui faça uma requisição com o axios e em seguida, no .then()
  //utilize a função dispatch() passando addDigimon(response.data.name)
};
export default addDigimonsThunk;
