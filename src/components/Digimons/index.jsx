import {
  Container,
  MainFrame,
  Stats,
  Border,
  Glass,
  Button,
  Buttons,
} from "./styles";

const Digimons = ({ digimon }) => {
  return (
    <Container>
      <Border />
      <MainFrame>
        <h4>{digimon.name}</h4>
        <img alt={digimon.name} src={digimon.img}></img>
        <Glass />
      </MainFrame>
      <Stats>
        <span>{digimon.level}</span>
      </Stats>
      <Buttons>
        <Button />
        <Button />
        <Button />
      </Buttons>
    </Container>
  );
};

export default Digimons;
