import styled, { keyframes } from "styled-components";

const appearFromNowhere = keyframes`
from{
    opacity: 0;
}

to{
    opacity: 1;
}
`;

export const Container = styled.div`
  font-family: "Orbitron", sans-serif;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  background-color: var(--red);
  color: var(--yellow);
  width: 290px;
  height: 360px;
  border-radius: 54%/65% 65% 47% 47%;
  border: 5px solid var(--stroke);
  box-shadow: inset 10px 0 rgba(255, 255, 255, 0.7),
    inset -10px 0 rgba(0, 0, 0, 0.2);
  position: relative;
  margin: 10px;
  animation: ${appearFromNowhere} 1s;
`;

export const MainFrame = styled.div`
  z-index: 1;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  position: relative;
  > img {
    border-radius: 12px;
    width: 150px;
    position: absolute;
  }
  > h4 {
    font-size: 14px;
    top: -20px;
    position: absolute;
    text-transform: uppercase;
  }
`;

export const Stats = styled.div`
  display: flex;
  justify-content: center;
`;

export const Border = styled.div`
  width: 100%;
  height: 8px;
  background-color: var(--red);
  border: 2px solid var(--stroke);
  display: block;
  position: absolute;
  z-index: 0;
  box-shadow: inset 12px 0 rgba(255, 255, 255, 0.7),
    inset -12px 0 rgba(0, 0, 0, 0.2);
`;

export const Glass = styled.div`
  width: 175px;
  height: 175px;
  background-color: rgba(128, 223, 168, 0.65);
  border: 6px solid var(--stroke);
  box-shadow: inset 6px 6px rgba(0, 0, 0, 0.2);
  overflow: hidden;
  position: relative;
  border-radius: 12px;

  :after,
  :before {
    content: "";
    height: 200%;
    width: 80px;
    background-color: rgba(255, 255, 255, 0.2);
    position: absolute;
    transform: rotate(45deg);
    top: -80px;
  }
  :after {
    right: -20px;
    width: 50px;
  }
`;

export const Button = styled.button`
  background-color: var(--yellow);
  border: 6px solid var(--stroke);
  border-radius: 50%;
  position: relative;
  width: 40px;
  height: 40px;
  padding: 0;
  box-shadow: inset 4px 0 rgba(255, 255, 255, 0.7),
    inset -4px 0 rgba(0, 0, 0, 0.2);
  margin: 0 5px;
  cursor: pointer;
  outline: 0;
  :focus {
    box-shadow: none;
  }
`;

export const Buttons = styled.div`
  display: flex;
  position: relative;
  top: 10px;

  button:nth-child(2) {
    top: 10px;
  }
`;
