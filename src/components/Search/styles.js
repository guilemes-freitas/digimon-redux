import styled, { css } from "styled-components";

export const Container = styled.div`
  input,
  button {
    margin-top: 15px;
    height: 30px;
    transition: all 0.4s;
  }
  button {
    border: 0;
    background-color: var(--yellow);
    color: var(--black);
    width: 90px;
    border-radius: 0 5px 5px 0;
    font-weight: bold;
  }
  input {
    border-radius: 5px 0 0 5px;
    padding-left: 15px;
    border: 2px solid var(--yellow);
  }

  p {
    color: var(--red);
  }
  ${(props) =>
    props.isErrored &&
    css`
      input {
        border-color: var(--red);
      }
      button {
        background-color: var(--red);
        color: var(--white);
      }
    `}
`;
