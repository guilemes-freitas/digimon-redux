import { useState } from "react";
import { useDispatch } from "react-redux";
import addDigimonsThunk from "../../store/modules/digimons/thunk";
import { Container } from "./styles";

const Search = () => {
  const [input, setInput] = useState("");
  const [error, setError] = useState(false);

  const dispatch = useDispatch();

  const handleSearch = () => {
    dispatch(addDigimonsThunk(input, setError));
    //chame o dispatch() passando o state input
    setInput("");
  };

  return (
    <Container isErrored={!!error}>
      <input
        value={input}
        onChange={(e) => {
          setInput(e.target.value);
          setError(false);
        }} // faça com que o onChange troque o setInput
        placeholder="Procure seu Digimon"
      ></input>
      <button onClick={handleSearch}>Pesquisar</button>
      {error && <p>Digimon não encontrado.</p>}
    </Container>
  );
};

export default Search;
