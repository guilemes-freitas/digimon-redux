import styled from "styled-components";

export const Container = styled.div`
  width: 90vw;
  display: flex;
  justify-content: flex-start;
  flex-wrap: wrap;
  align-items: flex-start;
`;
