import { useSelector } from "react-redux";
import Digimons from "../Digimons";
import { Container } from "./styles";

const DigimonList = () => {
  const { digimons } = useSelector((state) => state);
  return (
    <Container>
      {digimons.map((digimon, index) => {
        return <Digimons key={index} digimon={digimon} />;
      })}
    </Container>
  );
};

export default DigimonList;
